-- Gera��o de Modelo f�sico
-- Sql ANSI 2003 - brModelo.



CREATE TABLE emprestimo (
id_emprestimo INTEGER PRIMARY KEY AUTO_INCREMENT,
data_emprestimo DATETIME,
data_devolucao VARCHAR(10),
id_cliente INTEGER,
id_livro INTEGER
);

CREATE TABLE livro (
id_livro INTEGER PRIMARY KEY AUTO_INCREMENT,
exemplar VARCHAR(100),
autor VARCHAR(100),
edicao INTEGER,
ano INTEGER,
disponibilidade VARCHAR(1)
);

CREATE TABLE cliente (
nome VARCHAR(100),
data_nasc DATETIME,
sexo VARCHAR(10),
cpf VARCHAR(11),
endereco VARCHAR(100),
fone VARCHAR(10),
id_cliente INTEGER PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE multa (
id_multa  INTEGER PRIMARY KEY AUTO_INCREMENT,
descricao VARCHAR(60),
valor DECIMAL(10),
id_cliente INTEGER,
FOREIGN KEY(id_cliente) REFERENCES cliente (id_cliente) ON UPDATE RESTRICT ON DELETE RESTRICT
);

ALTER TABLE emprestimo ADD FOREIGN KEY(id_cliente) REFERENCES cliente (id_cliente) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE emprestimo ADD FOREIGN KEY(id_livro) REFERENCES livro (id_livro) ON UPDATE RESTRICT ON DELETE RESTRICT;
